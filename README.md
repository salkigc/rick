# README #

RICK - RNA Information Computing Kit

### What is this repository for? ###

* RICK (RNA Information Computing Kit) is a shiny web application which performs RNAseq analysis.
* RICK works on an expression table uploaded by the user
* RICK performs differential expression testing and generates hierarchically clustered heat maps and PCA plots to facilitate sample comparison as well pathay enrichment analysis to summarize differential expression testing.  

### How do I get set up? ###

* RICK can be launched locally if R is installed
* RICK is also available at rick.salk.edu

### How does RICK work? ###

RCK accepts as input a file with raw read counts for each transcript and sample.

Gene annotation is performed using the R package AnnotationDbi (Pagès H, 2017) and raw gene counts are normalized using the DESeq2 package (Love, Huber, & Anders, 2014) and transformed with the regularized-logarithm transformation (rlogTransformation in DESeq2) for visualization.

Hierarchical clustering is carried out based on the similarity of the top 1000 most highly expressed genes using the R hclust function. Principal Component Analysis (PCA) is an alternate approach for visualizing sample similarity. Top 500 genes with the highest variance from two groups (ex. Treatment and WT) are plotted according to the top two (2D plot) and three (3D plot) principal components.

To find significantly differentially expressed genes, two popular methods for differential expression analysis are provided: edgeR (McCarthy, Chen, & Smyth, 2012) and DESeq2 (Love et al., 2014) which estimate biological variability using replicate information and test for significant changes in gene expression between two groups of samples that are defined by the user. In addition, the edgeR and DESeq2 methods can be combined to identify genes that are differentially expressed using either the minimum, maximum or average adjusted p-value from edgeR and DESeq2, enabling permissive, conservative, or balanced identification of significantly different genes. The up and down-regulated genes can be downloaded as a table with adj. p-values and fold-changes.

Gene set analysis (GSA) compares the pre-defined gene set expression changes to the background set to identify pathways that are being perturbed. GSA does not require a user-defined hard cutoff for the expression significance making it more sensitive to pathway-wide coordinated expression changes. To check which KEGG (Kanehisa & Goto, 2000) or Gene Ontology (Ashburner et al., 2000) terms are undergoing expression changes in both directions, the GAGE R package (Luo, Friedman, Shedden, Hankenson, & Woolf, 2009) is used as it shows significantly better results when compared to two common GSA methods. To graphically visualize which genes within overrepresented KEGG pathways are changing, RICK uses the Pathview R package (Luo & Brouwer, 2013).

### References ###
1. Ashburner, M., Ball, C. A., Blake, J. A., Botstein, D., Butler, H., Cherry, J. M., . . . Sherlock, G. (2000). Gene ontology: tool for the unification of biology. The Gene Ontology Consortium. Nat Genet, 25(1), 25-29. doi:10.1038/75556

2. Kanehisa, M., & Goto, S. (2000). KEGG: kyoto encyclopedia of genes and genomes. Nucleic Acids Res, 28(1), 27-30.

3. Love, M. I., Huber, W., & Anders, S. (2014). Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2. Genome Biol, 15(12), 550. doi:10.1186/s13059-014-0550-8

4. Luo, W., & Brouwer, C. (2013). Pathview: an R/Bioconductor package for pathway-based data integration and visualization. Bioinformatics, 29(14), 1830-1831. doi:10.1093/bioinformatics/btt285

5. Luo, W., Friedman, M. S., Shedden, K., Hankenson, K. D., & Woolf, P. J. (2009). GAGE: generally applicable gene set enrichment for pathway analysis. BMC Bioinformatics, 10, 161. doi:10.1186/1471-2105-10-161

6. McCarthy, D. J., Chen, Y., & Smyth, G. K. (2012). Differential expression analysis of multifactor RNA-Seq experiments with respect to biological variation. Nucleic Acids Res, 40(10), 4288-4297. doi:10.1093/nar/gks042

7. Pagès H, C. M., Falcon S and Li N. (2017). AnnotationDbi: Annotation Database Interface.

8. Young, N. P., Kamireddy, A., Van Nostrand, J. L., Eichner, L. J., Shokhirev, M. N., Dayn, Y., & Shaw, R. J. (2016). AMPK governs lineage specification through Tfeb-dependent regulation of lysosomes. Genes & development, 30(5), 535-552.

### Who do I talk to? ###

* gerikson@salk.edu
